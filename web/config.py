import yaml

CONFIG = {}

with open("config.yaml") as f:
    CONFIG = yaml.safe_load(f.read())