from datetime import datetime
import sqlite3
import re

POSTFIX_REGEX = re.compile(r"postfix\/smtpd\[[0-9]*\]: ([A-Z0-9]*):")
SPAMD_REGEX = re.compile(r"result: (Y|\.) [0-9]*.*mid=<(.*)>")

PARSED_CODES = []

def get_connection():
    conn = sqlite3.connect("pla.db")
    conn.row_factory = sqlite3.Row
    return conn

def initialize():
    global PARSED_CODES
    PARSED_CODES.clear()

    conn = get_connection()

    for row in conn.execute("SELECT code FROM logs"):
        PARSED_CODES.append(row["code"])

    conn.close()

def find_entry_by_id(lines, code):
    # Message-Id, From, To, Spam
    from_ = None
    to = None
    msgid = None
    spam = None

    for line in lines:        

        # Postfix
        if code in line:
            if not msgid and "postfix/cleanup" in line:
                msgid = re.search(r"message-id=<(.*)>", line).groups()[0]
                continue

            if not from_ and "postfix/qmgr" in line:
                from_ = re.search(r"from=<(.*)>", line).groups()[0]
                continue

            if not to and "postfix/pipe" in line:
                to = re.search(r"to=<(.*)>", line).groups()[0]
                if "orig_to" in to:
                    to = re.search(r"orig_to=\<(.*)", to).groups()[0]
                continue

        # SpamAssassin
        if msgid and msgid in line:
            match = SPAMD_REGEX.search(line)
            if match:
                status, score = match.groups()
                spam = status == "Y"
                continue
        
        if from_ and to and msgid and spam is not None:
            return (msgid, from_, to, spam)
        
    return []
                

def parse_logs(filename, last_datetime):
    if not isinstance(last_datetime, datetime):
        ValueError("last_datetime must be a instance of datetime class")

    now = datetime.now()

    with open(filename, "r") as f:
        lines = f.readlines()

    filtered_lines = []
    for index, line in enumerate(lines):
        when = datetime.strptime(f"{now.year} {line[:15]}", "%Y %b %d %H:%M:%S")
        if when > last_datetime:
            filtered_lines = lines[index:]
            break
        
    conn = get_connection() 
    try:   
        for index, line in enumerate(filtered_lines):
            match = POSTFIX_REGEX.search(line)
            if match:
                code = match.groups()[0]
                if code not in PARSED_CODES:
                    entry = find_entry_by_id(filtered_lines[index:], code)
                    if entry:
                        PARSED_CODES.append(code)
                        when = datetime.strptime(f"{now.year} {line[:15]}", "%Y %b %d %H:%M:%S")
                        _, sender, recipient, status = entry
                        conn.execute("INSERT INTO logs (code, sender, recipient, status, created_at) VALUES (?, ?, ?, ?, ?)", [code, sender, recipient, status, when])
        conn.commit()
    finally:
        conn.close()

    return filtered_lines