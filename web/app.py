from flask import Flask, request, jsonify, session
from datetime import datetime, timedelta
from threading import Thread
import time
from pypika import Query, Table
from pypika.enums import Order
from functools import wraps

from .lib import parse_logs, initialize, get_connection
from .config import CONFIG

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]ssssas/'


def worker():
    last_check = datetime.now()
    while True:        
        try:
            print("Parsing...")
            parse_logs(CONFIG["syslog_filename"], last_check - timedelta(hours=1))
        except Exception as e:
            print(str(e))
        last_check = datetime.now()
        time.sleep(5)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if "username" not in session:
            return ("", 401)
        return f(*args, **kwargs)
    return decorated_function

@app.before_first_request
def before_first_request_func():
    print("Initializing...")
    initialize()

    thread = Thread(target=worker)
    thread.start()

@app.route("/api/logs")
@login_required
def get_logs():    
    search = request.args.get("q", "", str)
    time = datetime.fromtimestamp(request.args.get("time", datetime.now().timestamp(), str))
    amount = request.args.get("amount", 20, int)
    status = request.args.get("status", "any")
    kind = request.args.get("kind", "inbox")

    Logs = Table("logs")
    query = Query.from_("logs").select("*")

    if search:
        if kind == "inbox":
            query = query.where((Logs.sender == search)  | (Logs.recipient == search))
        elif kind == "domain":
            like_value = f"%@{search}%"
            query = query.where(Logs.sender.like(like_value)  | Logs.recipient.like(like_value))

    if time:
        query = query.where(Logs.created_at < time.isoformat())

    if status != "any":
        query = query.where(Logs.status == (1 if status == "spam" else 0))


    query = query.limit(amount).orderby(Logs.created_at, order=Order.desc)


    db = get_connection()
    sql = query.get_sql()
    print(sql)
    logs = [dict(log) for log in db.execute(sql)]

    return jsonify({"logs": logs})

@app.route("/api/login", methods=["POST"])
def login():
    data = request.get_json()
    if request.method == "POST":
        session["username"] = data["username"]
        return ("", 200)

@app.route("/api/whoiam")
@login_required
def whoiam():
    if "username" in session:
        return jsonify({"username": session["username"]})

@app.route("/api/logout")
def logout():
    session.pop("username", None)
    return ("", 204)